# A Bash script to list network interfaces to be used in whiptail

A project to be inserted in this project by Le Garage numérique: https://gitlab.com/garagenum/linux-admin-tools/debian-customizer

The script displays network interfaces and their IP addresses in a Whiptail menu.
